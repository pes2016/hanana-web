'use strict';

/**
 * @ngdoc function
 * @name hananaWebApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the hananaWebApp
 */
angular.module('hananaWebApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
