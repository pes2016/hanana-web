(function() {
    'use strict';

    angular.module('hananaWebApp')
    .factory('BadgeFactory', ['$http', '$q', '$location', 'Upload',
    function ($http, $q, $location, Upload) {

        return {

            getAdminBadges: function() {
                var prom = $q.defer();

                $http.get('/api/admin/badges')
                    .then(function (response) {
                        prom.resolve(response.data);
                    },function(err) {
                        prom.reject(err.data);
                    });

                return prom.promise;
            },

            getPremiumBadges: function() {
                var prom = $q.defer();

                $http.get('/api/premium/badges')
                    .then(function (response) {
                        prom.resolve(response.data);
                    },function(err) {
                        prom.reject(err.data);
                    });

                return prom.promise;
            },

            getAdminBadge: function(id) {
                var prom = $q.defer();
                $http.get('/api/admin/badges/' + id)
                    .then(function (response) {
                        prom.resolve(response.data);
                    },function(err) {
                        prom.reject(err.data);
                    });
                return prom.promise;
            },

            editBadge: function(badge) {
                var prom = $q.defer();
                $http.put('/api/admin/badges/' + badge._id, badge)
                    .then(function () {
                        prom.resolve();
                    }, function(response){
                        prom.reject(response.data);
                    });
                return prom.promise;
            },

            editAdminBadge: function(badge) {
                var prom = $q.defer();

                Upload.upload({
					url: '/api/admin/badges/' + badge._id,
					method: 'POST',
					data: badge
				}).then(function () {
                    prom.resolve();
                }, function(response) {
                    prom.reject(response.data);
                });

                return prom.promise;
            },

            createAdminBadge: function(badge) {
                var prom = $q.defer();

                Upload.upload({
					url: '/api/admin/badges',
					method: 'POST',
					data: badge
				}).then(function () {
                    prom.resolve();
                }, function(response) {
                    prom.reject(response.data);
                });

                return prom.promise;
            },

            createPremiumBadge: function(badge) {
                var prom = $q.defer();

                Upload.upload({
					url: '/api/premium/badges',
					method: 'POST',
					data: badge
				}).then(function () {
                    prom.resolve();
                }, function(response) {
                    prom.reject(response.data);
                });

                return prom.promise;
            },

            getAdminTriggers: function() {
                var prom = $q.defer();

                $http.get('/api/admin/triggers/')
                    .then(function (response) {
                        prom.resolve(response.data);
                    }, function(response){
                        prom.reject(response.data);
                    });
                return prom.promise;
            },

            getPremiumTriggers: function() {
                var prom = $q.defer();

                $http.get('/api/premium/triggers/')
                    .then(function (response) {
                        prom.resolve(response.data);
                    }, function(response){
                        prom.reject(response.data);
                    });
                return prom.promise;
            }

        };
    }]);
})();
