(function() {
    'use strict';

    angular.module('hananaWebApp')
    .controller('createBadgePremiumCtrl', [ '$scope', '$location', '$http','BadgeFactory','GroupFactory','EventFactory', '$translate',
    function ($scope, $location, $http, BadgeFactory, GroupFactory,EventFactory, $translate ) {

        var requiredFields = ['name', 'description', 'image', 'trigger', 'triggerValue', 'points'];

        function errorText(msg, field) {
            $scope.errorText = msg;
            if (!field) {
                $scope.formClass = 'has-error';
            } else {
                $scope[field+'Class'] = 'has-error';
            }
            $scope.errorClass = 'has-error';
        }
        function success() {
            $location.path('/premium/badges');
        }

        $scope.title = 'Crear badge';
        $scope.formClass = '';

        $scope.build = $scope.create = function () {
            $scope.formClass = '';

            var incomplete = false;
            angular.forEach(requiredFields, function (rf) {
                if (!$scope.badge || !$scope.badge[rf]) {
                    errorText($translate.instant('commons.fillAllFields'),rf);
                    incomplete = true;
                } else {
                    $scope[rf+'Class'] = '';
                }
            });

            if (!$scope.badge.event && !$scope.badge.group) {
                incomplete = true;
            }
            if (incomplete) {
                return;
            }

            BadgeFactory.createPremiumBadge($scope.badge)
            .then(function () {
                success();
            }, function(err) {
                errorText(err);
            });
        };

        $scope.checkTrigger = function (trigger) {
            if (trigger===7) {
                $scope.triggerAllowsQuant = false;
                $scope.badge.triggerValue = 1;
            } else {
                $scope.triggerAllowsQuant = true;
            }
            console.log($scope.badge.trigger);
        };

        $scope.getTrigger = function (triggerId) {
            var trigger;
            angular.forEach($scope.triggers, function(t) {
                if (t.id === triggerId) {
                    trigger = t;
                }
            });
            return trigger;
        };

        BadgeFactory.getPremiumTriggers()
        .then(function(triggers){
            $scope.triggers = triggers;
        },function(err){
            errorText(err);
        });

        GroupFactory.getOwnedGroups()
        .then(function(groups){
            $scope.groups = groups;
        },function(err){
            errorText(err);
        });

        EventFactory.getEvents()
        .then(function(events){
            $scope.events = events;
        },function(err){
            errorText(err);
        });
    }]);
})();
