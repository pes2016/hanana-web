(function()  {
	'use strict';

	angular.module('hananaWebApp')
	.controller('createGroupCtrl', ['$scope', '$location', 'GroupFactory',
	function($scope, $location, GroupFactory) {

		function errorText (msg) {
			$scope.errorText = msg;
			$scope.formClass = 'has-error';
		}

		function success () {
			$scope.formClass = 'has-success';
			$location.path('/home');
		}

		$scope.formClass = '';
		$scope.submit = function() {
			GroupFactory.createGroup($scope.name, $scope.description, $scope.file)
			.then(function () {
				success();
			}, function(err) {
				errorText(err);
			});
		};
	}]);
})();
