(function() {
    'use strict';

    angular.module('hananaWebApp')
    .controller('createBadgeAdminCtrl', [ '$scope', '$location', '$http','BadgeFactory', '$translate',
    function ($scope, $location, $http, BadgeFactory, $translate) {

        var requiredFields = ['name','description','image','points'];

        function errorText(msg, field) {
            $scope.errorText = msg;
            if (!field) {
                $scope.formClass = 'has-error';
            } else {
                $scope[field+'Class'] = 'has-error';
            }
            $scope.errorClass = 'has-error';
        }
        function success() {
            $location.path('/admin/badges');
        }

        $scope.formClass = '';

        $scope.build = $scope.create = function () {
            $scope.formClass = '';

            var incomplete = false;
            angular.forEach(requiredFields, function (rf) {
                if (!$scope.badge || !$scope.badge[rf]) {
                    errorText($translate.instant('commons.fillAllFields'),rf);
                    incomplete = true;
                } else {
                    $scope[rf+'Class'] = '';
                }
            });
            if (incomplete) {
                return;
            }

            BadgeFactory.createAdminBadge($scope.badge)
            .then(function () {
                success();
            }, function(err) {
                errorText(err);
            });
        };

        BadgeFactory.getAdminTriggers()
        .then(function(triggers){
            $scope.triggers = triggers;
        },function(err){
            errorText(err);
        });
    }]);
})();
