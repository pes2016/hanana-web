'use strict';

/**
 * @ngdoc overview
 * @name hananaWebApp
 * @description
 * # hananaWebApp
 *
 * Main module of the application.
 */
angular
    .module('hananaWebApp', [
        'ngRoute',
        'ngFileUpload',
        'pascalprecht.translate'
    ])
    .constant('LOCALES', {
        'locales': {
        'es': 'Español',
        'en': 'English'
        }
    })
    .config(function ($translateProvider) {
        $translateProvider.useStaticFilesLoader({
            prefix: '/locales/',
            suffix: '.json'
        });
        $translateProvider.preferredLanguage('en');
    })
    .config(function ($routeProvider) {

        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                controllerAs: 'main'
            })
            .when('/login', {
                templateUrl: 'views/login.html',
                controller: 'loginCtrl',
                controllerAs: 'login'
            })
            .when('/about', {
                templateUrl: 'views/about.html',
                controller: 'AboutCtrl',
                controllerAs: 'about'
            })
            .when('/contact', {
                templateUrl: 'views/contact.html',
                controller: 'ContactCtrl',
                controllerAs: 'contact'
            })
            .when('/logout', {
                templateUrl: 'views/logout.html',
                controller: 'logoutCtrl',
                controllerAs: 'logout'
            })
            .when('/admin/createUserPremium', {
                templateUrl: 'views/admin/createUserPremium.html',
                controller: 'createUserPremiumCtrl',
                controllerAs: 'createUserPremium'
            })
            .when('/admin/premium', {
                templateUrl: 'views/admin/premiumList.html',
                controller: 'premiumListCtrl',
                controllerAs: 'premiumUserlist'
            })
            .when('/admin/premium/edit/:id', {
                templateUrl: 'views/admin/premiumEdit.html',
                controller: 'premiumEditCtrl',
                controllerAs: 'epu'
            })
            .when('/admin/badges/', {
                templateUrl: 'views/admin/badgeList.html',
                controller: 'listBadgesAdminCtrl',
                controllerAs: 'ctrl'
            })
            .when('/premium/badges/', {
                templateUrl: 'views/premium/badgeList.html',
                controller: 'listBadgesPremiumCtrl',
                controllerAs: 'ctrl'
            })
            .when('/admin/badges/create', {
                templateUrl: 'views/admin/badgeForm.html',
                controller: 'createBadgeAdminCtrl',
                controllerAs: 'ctrl'
            })
            .when('/premium/badges/create', {
                templateUrl: 'views/premium/badgeForm.html',
                controller: 'createBadgePremiumCtrl',
                controllerAs: 'ctrl'
            })
            .when('/admin/badges/edit/:id', {
                templateUrl: 'views/admin/badgeForm.html',
                controller: 'editBadgeAdminCtrl',
                controllerAs: 'ctrl'
            })
            .when('/premium/createGroup', {
                templateUrl: 'views/premium/createGroup.html',
                controller: 'createGroupCtrl',
                controllerAs: 'createGroup'
            })
            .when('/premium/groups', {
                templateUrl: 'views/premium/groupPremiumList.html',
                controller: 'groupPremiumListCtrl',
                controllerAs: 'listPremiumGroups'
            })
            .when('/premium/events/create', {
                templateUrl: 'views/events/createEvent.html',
                controller: 'createEventCtrl',
                controllerAs: 'createEvent'
            })
            .when('/premium/events/edit/:id', {
                templateUrl: 'views/events/createEvent.html',
                controller: 'editEventCtrl',
                controllerAs: 'editEvent'
            })
            .when('/premium/groups/edit/:id', {
                templateUrl: 'views/premium/groupEdit.html',
                controller: 'groupEditCtrl',
                controllerAs: 'groupEdit'
            })
            .when('/premium/events', {
                templateUrl: 'views/events/listEvents.html',
                controller: 'listEventsCtrl',
                controllerAs: 'listEvents'
            })
            .when('/admin/complaints', {
                templateUrl: 'views/admin/complaintList.html',
                controller: 'listComplaintsCtrl',
                controllerAs: 'ctrl'
            })
            .when('/admin/notifications', {
                templateUrl: 'views/admin/notificationsAdmin.html',
                controller: 'adminNotificationsCtrl',
                controllerAs: 'ctrl'
            })
            .when('/premium/notifications', {
                templateUrl: 'views/premium/notificationsPremium.html',
                controller: 'premiumNotificationsCtrl',
                controllerAs: 'ctrl'
            })
            .otherwise({
                redirectTo: '/'
            });
  });
