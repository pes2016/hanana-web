
'use strict';
window.mapProm = $.Deferred();

/* jshint ignore:start */
function initMap() {
    mapProm.resolve();
}
/* jshint ignore:end */

(function() {

    var MapController = ['$scope', '$rootScope', function($scope,$rootScope) {

        var map;
        var places;
        var autocomplete;
        $scope.inputId = 'mapCity';
        $scope.mapId = 'map';
        $scope.initMap = function initMap(element) {
            window.mapProm.then(function() {
                map = new google.maps.Map(element.find('#'+$scope.mapId)[0], {
                    zoom: 5,
                    center: {lat: 40.5, lng: -3.7},
                    mapTypeControl: true,
                    panControl: true,
                    zoomControl: true,
                    streetViewControl: true
                });

                // Create the autocomplete object and associate it with the UI input control.
                // Restrict the search to the default country, and to place type "cities".
                autocomplete = new google.maps.places.Autocomplete(
                    $('#' + $scope.inputId)[0], {});
                places = new google.maps.places.PlacesService(map);

                function onPlaceChanged() {
                    var place = autocomplete.getPlace();
                    if (place.geometry) {
                        map.panTo(place.geometry.location);
                        map.setZoom(15);
                    } else {
                        $('#' + $scope.inputId).placeholder = 'Introduce una ubicación';
                    }
                }

                autocomplete.addListener('place_changed', onPlaceChanged);
                $rootScope.$on('placeSet', function(ev,loc) {
                    map.panTo({lng: parseFloat(loc.lon,10), lat: parseFloat(loc.lat,10)});
                    map.setZoom(15);
                    $scope.marker = new google.maps.Marker({
                        position: {lng: parseFloat(loc.lon,10), lat: parseFloat(loc.lat,10)},
                        map: map
                    });
                });

                map.addListener('click', function (event) {
                    if ($scope.marker) {
                        $scope.marker.setPosition(event.latLng);
                    } else {
                        $scope.marker = new google.maps.Marker({
                            position: event.latLng,
                            map: map
                        });
                    }
                    $rootScope.$broadcast('placeChanged', event.latLng);
                });
            });


        };


    }];

    angular.module('hananaWebApp')
    .directive('hananaMap', [
    function () {

        return {
            restrict: 'E',
            scope: {
                inputId: '@'
            },
            controller: MapController,
            templateUrl: 'views/map.html',
            link: function (scope, element) {
                scope.initMap(element);
            }
        };
    }]);
})();
