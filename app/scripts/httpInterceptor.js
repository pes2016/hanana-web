(function() {
  'use strict';

    angular.module('hananaWebApp')
    .factory('httpIntercep', ['$window', '$q', '$injector', function($window, $q, $injector) {

        var httpIntercep = {
            request: function(config) {
                var char = '?';
                if (config.url.indexOf('?') > 0) {
                    char = '&';
                }
                var auth = $injector.get('AuthService');
                var lang = $injector.get('LocaleService');
                config.url += char + auth.getAC() + '&' + lang.getLocaleHeader();
                return config;
            },

            responseError: function(response) {
                if (response.status === 401){
                    var $http = $injector.get('$http');
                    var deferred = $q.defer();
                    var auth = $injector.get('AuthService');
                    var url = response.config.url;
                    var ind = url.length;
                    var ampInd = (url.lastIndexOf('&') > -1) ? url.lastIndexOf('&') : url.lastIndexOf('?');
                    var token = url.slice(ampInd, ind);
                    response.config.url = url.replace(token,'');
                    auth.refresh().then(deferred.resolve, deferred.reject);
                    return deferred.promise.then(function() {
                        return $http(response.config);
                    });
                }
                return $q.reject(response);
            }
        };

        return httpIntercep;
    }])
    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.interceptors.push('httpIntercep');
    }]);
})();
