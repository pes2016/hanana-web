# Hanana Web

**How to start:**

- Clone the git project and switch to develop branch.
- Use `npm install` to install project development modules and bower.
- Use `bower install` to install project modules. If you haven't installed development modules make sure bower is installed.
- Use `grunt serve` to start a server with grunt or search `localhost:4000/` in a browser to access the web app.
- /app and /bower_components **must be in the same directory** (related to web project).
- To get all dependencies and a seed mongo run `./install` from backend.

**Project execution order:**

1. Execute mongod.
2. Start the backend server.
3. Access to server with the corresponding app.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
