(function() {
	'use strict';

	angular.module('hananaWebApp')
	.factory('GroupFactory', ['$q', '$http', 'AuthService', 'Upload',
	function ($q, $http, AuthService, Upload) {

		return {

			createGroup: function(name, description, image) {
				var prom = $q.defer();
				Upload.upload({
					url: '/api/premium/createGroup',
					method: 'POST',
					data: {
						file: image,
						name: name,
						description: description
					}
				}).then(function () {
					prom.resolve();
				}, function(response) {
					prom.reject(response.data);
				});
				return prom.promise;
			},

			getOwnedGroups: function() {
				var prom = $q.defer();
				$http.get('/api/premium/getOwnedGroups', {})
                    .then(function (response) {
                        prom.resolve(response.data);
                    }, function(err){
                        prom.reject(err.data);
                    });
                return prom.promise;
			},

			getOwnedGroup: function(id) {
                var prom = $q.defer();
                $http.get('/api/premium/getOwnedGroup/' + id)
                    .then(function (response) {
                        prom.resolve(response.data);
					},function(err) {
						prom.reject(err.data);
					});
				return prom.promise;
            },

			editGroup: function(group, imageWasModified) {
                var prom = $q.defer();
				if(imageWasModified) {
					Upload.upload({
						url: '/api/premium/modifyGroup/' + group._id,
						method: 'POST',
						data: {
							file: group.image,
							name: group.name,
							description: group.description
						}
					}).then(function () {
							prom.resolve();
						}, function(err) {
							prom.reject(err.data);
					});
				} else {
					$http.post('/api/premium/modifyGroup/' + group._id, group)
                        .then(function () {
                            prom.resolve();
                        }, function(response){
                            prom.reject(response.data);
						});
				}
                return prom.promise;
            },

			deleteGroup: function(id) {
				var prom = $q.defer();
                $http.delete('/api/premium/removeGroup/' + id)
                    .then(function () {
                        prom.resolve();
					}, function(response){
						prom.reject(response.data);
					});
                return prom.promise;
            },

		};
	}]);
})();
