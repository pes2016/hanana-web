'use strict';

/**
 * @ngdoc function
 * @name hananaWebApp.controller:ContactCtrl
 * @description
 * # ContactCtrl
 * Controller of the hananaWebApp
 */
angular.module('hananaWebApp')
  .controller('ContactCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
