(function() {
    'use strict';

    angular.module('hananaWebApp')
    .factory('NotificationFactory', ['$http', '$q',
    function ($http, $q) {

        return {

            createNotificationAdmin: function(title, description) {
                const NOTIFICATION_URL = '/api/admin/notify';
                var prom = $q.defer();
                $http({
                    method: 'POST',
                    url: NOTIFICATION_URL,
                    data: {
                        title: title,
                        body: description
                    },
                }).then(function () {
                    prom.resolve();
                }, function(err) {
                    prom.reject(err.data);
                });
                return prom.promise;
            },

            createNotificationPremium: function(title, description, group) {
                const NOTIFICATION_URL = '/api/premium/notify';
                var prom = $q.defer();
                $http({
                    method: 'POST',
                    url: NOTIFICATION_URL,
                    data: {
                        title: title,
                        body: description,
                        extras: {
                            group: group
                        }
                    }
                }).then(function () {
                    prom.resolve();
                }, function(err) {
                    prom.reject(err.data);
                });
                return prom.promise;
            }

        };
    }]);
})();
