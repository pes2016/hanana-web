(function() {
    'use strict';

    angular.module('hananaWebApp')
    .factory('EventFactory', ['$http', '$q', '$location', 'AuthService', 'Upload',
    function ($http, $q, $location, AuthService, Upload) {

        return {

            getEvents: function() {
                var prom = $q.defer();

                $http.get('/api/premium/getOwnedEvents')
                    .then(function (response) {
                        prom.resolve(response.data);
                    },function(err) {
                        prom.reject(err.data);
                    });

                return prom.promise;
            },

            deleteEvent: function(id) {
                var prom = $q.defer();

                $http.delete('/api/premium/removeEvent/' + id )
                    .then(function () {
                        prom.resolve();
                    }, function(err) {
                        prom.reject(err.data);
                    });

                return prom.promise;
            },

            getEvent: function(id) {
                var prom = $q.defer();
                $http.get('/api/premium/getOwnedEvent/' + id)
                    .then(function (response) {
                        prom.resolve(response.data);
                    }, function(err) {
                        prom.reject(err.data);
                    });
                return prom.promise;
            },

            editEvent: function(event) {
                var prom = $q.defer();
                Upload.upload({
					url: '/api/events/' + event._id,
					method: 'POST',
					data: event
				}).then(function () {
                    prom.resolve();
                }, function(response) {
                    prom.reject(response.data);
                });
                return prom.promise;
            },

            downloadQR: function(id) {
                var prom = $q.defer();
                $http.get('/api/premium/getQR/' + id)
                    .then(function (response) {
                        prom.resolve(response.data);
                    }, function(err) {
                        prom.reject(err.data);
                    });
                return prom.promise;
            },

            createEvent: function(event) {
                var prom = $q.defer();

                Upload.upload({
					url: '/api/premium/createEvent',
					method: 'POST',
					data: event
				}).then(function () {
                    prom.resolve();
                }, function(response) {
                    prom.reject(response.data);
                });

                return prom.promise;
            }

        };
    }]);
})();
