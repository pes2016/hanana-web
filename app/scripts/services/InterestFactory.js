/**
 * Created by marcos on 12/01/2017.
 */

(function() {
  'use strict';

  angular.module('hananaWebApp')
    .factory('InterestFactory', ['$http', '$q',
      function ($http, $q) {

        return {
          getInterests: function() {
            const URL = '/api/interests';
            var prom = $q.defer();
            $http({
              method: 'GET',
              url: URL
            }).then(function (response) {
              prom.resolve(response.data);
          }, function(err) {
              prom.reject(err.data);
            });
            return prom.promise;
          }

        };
      }]);
})();
