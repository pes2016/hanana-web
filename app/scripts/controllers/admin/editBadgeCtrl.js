(function() {
    'use strict';

    angular.module('hananaWebApp')
    .controller('editBadgeAdminCtrl', [ '$scope', '$location', '$http','BadgeFactory', '$routeParams', '$translate',
    function ($scope, $location, $http, BadgeFactory, $routeParams, $translate) {

        var requiredFields = ['name','description','image','points'];

        function errorText(msg, field) {
            $scope.errorText = msg;
            if (!field) {
                $scope.formClass = 'has-error';
            } else {
                $scope[field+'Class'] = 'has-error';
            }
            $scope.errorClass = 'has-error';
        }
        function success() {
            $location.path('/admin/badges');
        }

        $scope.formClass = '';

        $scope.build = $scope.edit = function () {
            $scope.formClass = '';

            var incomplete = false;
            angular.forEach(requiredFields, function (rf) {
                if (!$scope.badge || !$scope.badge[rf]) {
                    errorText($translate.instant('commons.fillAllFields'),rf);
                    incomplete = true;
                } else {
                    $scope[rf+'Class'] = '';
                }
            });
            if (incomplete) {
                return;
            }

            BadgeFactory.editAdminBadge($scope.badge)
            .then(function () {
                success();
            }, function(err) {
                errorText(err);
            });
        };

        BadgeFactory.getAdminBadge($routeParams.id)
        .then(function(badge) {
            $scope.badge = badge;
        },function(err){
            errorText(err);
        });

        BadgeFactory.getAdminTriggers()
        .then(function(triggers){
            $scope.triggers = triggers;
        },function(err){
            errorText(err);
        });
    }]);
})();
