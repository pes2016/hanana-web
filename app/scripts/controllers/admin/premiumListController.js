(function() {
    'use strict';

    angular.module('hananaWebApp')
    .controller('premiumListCtrl', [ '$scope', '$location', 'UserFactory',
    function ($scope, $location, UserFactory) {

        function getDateFromObjectId(id) {
            var timestamp = id.substring(0,8);
            return new Date( parseInt( timestamp, 16 ) * 1000);
        }

        $scope.users = [];
        $scope.sortAttribute = null;
        $scope.sortReverse = false;
        $scope.sortBy = function(propertyName) {
            $scope.sortReverse = ($scope.sortAttribute === propertyName) ? !$scope.sortReverse : false;
            $scope.sortAttribute = propertyName;
        };

        $scope.editUser = function(id) {
            $location.path('/admin/premium/edit/' + id);
        };

        $scope.deleteUser = function(user) {
            user.deleting = true;
        };
        $scope.deleteUserConfirm = function(id) {
            UserFactory.deletePremium(id)
            .then(function(){
                var userIndex = $scope.users.findIndex(function(user){
                    return user._id === id;
                });
                $scope.users.splice(userIndex,1);
            });
        };
        $scope.deleteUserDeny = function(user) {
            user.deleting = false;
        };

        UserFactory.getPremiums()
        .then(function(users){
            $scope.users = users;
            angular.forEach(users,function(user) {
                var date = getDateFromObjectId(user._id).toLocaleDateString();
                user.creationDate = date;
                user.deleting = false;
            });
        },function(){
            $location.path('/home');
        });
    }]);
})();
