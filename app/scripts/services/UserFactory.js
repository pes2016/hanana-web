(function() {
    'use strict';

    angular.module('hananaWebApp')
    .factory('UserFactory', ['$http', '$q', '$location', 'AuthService',
    function ($http, $q) {

        return {

            getPremiums: function() {
                var prom = $q.defer();
                $http.get('/api/admin/premiumUsers')
                    .then(function (response) {
                        prom.resolve(response.data);
                    },function(err) {
                        prom.reject(err.data);
                    });
                return prom.promise;
            },

            getPremium: function(id) {
                var prom = $q.defer();
                $http.get('/api/admin/premiumUsers/' + id)
                    .then(function (response) {
                        prom.resolve(response.data);
                    },function(err) {
                        prom.reject(err.data);
                    });
                return prom.promise;
            },

            editPremium: function(user) {
                var prom = $q.defer();
                $http.post('/api/admin/premiumUsers/' + user._id, user)
                    .then(function () {
                        prom.resolve();
                    }, function(response){
                        prom.reject(response.data);
                    });
                return prom.promise;
            },

            createPremium: function(name, email) {
                var prom = $q.defer();
                $http({
                    method: 'POST',
                    url: '/api/admin/registerPremium',
                    data: {
                        name: name,
                        email: email
                    },
                }).then(function () {
                    prom.resolve();
                }, function(response) {
                    prom.reject(response.data);
                });
                return prom.promise;
            },

          deletePremium: function(id) {
              var prom = $q.defer();
                $http.delete('/api/admin/removePremiumUser/' + id)
                    .then(function () {
                        prom.resolve();
                    },function(err) {
                        prom.reject(err.data);
                    });
                return prom.promise;
            },

            resetPassword: function(id) {
                var prom = $q.defer();
                $http.post('/api/admin/resetPassword/' + id, {})
                    .then(function () {
                        prom.resolve();
                    }, function(response){
                        prom.reject(response.data);
                    });
                return prom.promise;
            }

        };
    }]);
})();
