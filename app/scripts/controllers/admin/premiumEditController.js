(function() {
    'use strict';

    angular.module('hananaWebApp')
    .controller('premiumEditCtrl', [ '$scope', '$location', '$routeParams', 'UserFactory', '$translate',
    function ($scope, $location, $routeParams, UserFactory, $translate) {

        function errorText(msg) {
            $scope.errorText = msg;
            $scope.formClass = 'has-error';
        }
        function successText(msg) {
            $scope.errorText = msg;
            $scope.formClass = 'has-success';
        }

        $scope.user = [];
        $scope.save = function() {
            UserFactory.editPremium($scope.user)
            .then(function(){
                $location.path('/admin/premium');
            },function(err){
                errorText(err);
            });
        };
        $scope.resetPass = function() {
            UserFactory.resetPassword($scope.user._id)
            .then(function(){
                successText($translate.instant('scripts.controllers.premiumEditController.resetPassword'));
            },function(err){
                errorText(err);
            });
        };

        UserFactory.getPremium($routeParams.id)
        .then(function(user){
            $scope.user = user;
            $scope.user.isActive = (user.isActive) ? 1 : 0;
        },function(){
            $location.path('/home');
        });
    }]);
})();
