(function() {
    'use strict';

    angular.module('hananaWebApp')
    .controller('createEventCtrl', [ '$scope', '$rootScope', '$location', '$http', 'EventFactory', 'GroupFactory', '$translate', 'InterestFactory',
    function ($scope, $rootScope, $location, $http, EventFactory, GroupFactory, $translate, InterestFactory) {

        var requiredFields = ['title','description','lat','lon','date','tags','group','image','url'];

        InterestFactory.getInterests()
            .then(
            function (interests) {
                $scope.interests = interests;
            },
            function () {
            }
        );

        function errorText(msg, field) {
            $scope.errorText = msg;
            if (!field) {
                $scope.formClass = 'has-error';
            } else {
                $scope[field+'Class'] = 'has-error';
            }
            $scope.errorClass = 'has-error';
        }
        function success() {
            $scope.formClass = 'has-success';
            $scope.errorClass = 'has-success';
            $location.path('/premium/events');
        }

        $scope.create = true;
        $scope.formClass = '';
        $scope.event = {};
        $scope.build = $scope.create = function () {
            $scope.formClass = '';

            if (!$scope.event.description || !$scope.event.lat || !$scope.event.lon || !$scope.event.date ||
                !$scope.event.tags || !$scope.event.group || !$scope.event.image || !$scope.event.url) {
                angular.forEach(requiredFields, function (rf) {
                    if (!$scope.event || !$scope.event[rf]) {
                        errorText($translate.instant('commons.fillAllFields'),rf);
                    } else {
                        $scope[rf+'Class'] = '';
                    }
                });
                return;
            }

            var d = new Date($scope.event.date);
            if (d && d.getTime() < new Date().getTime()) {
                $scope.errorText = $translate.instant('scripts.controllers.createEventController.pastDate');
                $scope.dateError = 'has-error';
                $scope.errorClass = 'has-error';
                return;
            }

            var event = {
                title: $scope.event.title,
                description: $scope.event.description,
                date: $scope.event.date,
                tags: $scope.event.tags,
                group: $scope.event.group,
                image: $scope.event.image,
                qr: $scope.event.qr,
                lon: $scope.event.lon,
                lat: $scope.event.lat,
                url: $scope.event.url,
                address: $scope.event.address,
                price: $scope.event.price,
                age: $scope.event.age,
                interests : $scope.interests
            };

            EventFactory.createEvent(event)
            .then(function () {
                success();
            }, function(err) {
                errorText(err);
            });
        };

        $rootScope.$on('placeChanged',function(event,loc) {
            $scope.event.lat = loc.lat();
            $scope.event.lon = loc.lng();
        });

        GroupFactory.getOwnedGroups()
        .then(function(groups){
            $scope.groups = groups;
        },function(){
            $location.path('/home');
        });
    }]);
})();
