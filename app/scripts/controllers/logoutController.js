(function() {
    'use strict';
    angular.module('hananaWebApp')
    .controller('logoutCtrl', ['$scope', '$location', 'AuthService',
    function ($scope, $location, AuthService) {
          $scope.logoutUser = function () {
              AuthService.logout();
              $location.path('/login');
          };
          $scope.redirect = function () {
              $location.path('/home');
          };
    }]);
})();
