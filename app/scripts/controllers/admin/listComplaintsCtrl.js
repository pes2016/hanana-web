(function() {
    'use strict';

    angular.module('hananaWebApp')
    .controller('listComplaintsCtrl', [ '$scope', '$location', 'ComplaintFactory',
    function ($scope, $location, ComplaintFactory) {

        function errorText(msg) {
            $scope.errorText = msg;
            $scope.errorClass = 'has-error';
        }

        $scope.page = 0;

        function getComplaint() {
            ComplaintFactory.getComplaints($scope.page)
            .then(function(response) {
                if (!response.complaint) {
                    $scope.page = 0;
                    $scope.complaint = undefined;
                    $scope.reportedObject = undefined;
                    $scope.total = 0;
                } else {
                    if ($scope.page >= response.total) {
                        $scope.page = response.total -1;
                    }

                    $scope.complaint = response.complaint;
                    $scope.reportedObject = response.complaint.denounced;
                    $scope.total = response.total;
                }
            }, function() {
            });
        }

        $scope.next = function() {
            ++$scope.page;
            getComplaint();
        };

        $scope.back = function() {
            --$scope.page;
            getComplaint();
        };

        $scope.discard = function() {
            ComplaintFactory.discardComplaint($scope.complaint._id)
            .then(function() {
                getComplaint();
            }, function(err) {
                 errorText(err);
            });
        };

        $scope.accept = function() {
            ComplaintFactory.acceptComplaint($scope.complaint._id)
            .then(function() {
                getComplaint();
            }, function(err) {
                 errorText(err);
            });
        };

        getComplaint();

    }]);
})();
