(function() {
    'use strict';

    angular.module('hananaWebApp')
    .controller('listBadgesAdminCtrl', [ '$scope', '$location', 'BadgeFactory',
    function ($scope, $location, BadgeFactory) {

        function getDateFromObjectId(id) {
            var timestamp = id.substring(0,8);
            return new Date( parseInt( timestamp, 16 ) * 1000);
        }

        $scope.badges = [];
        $scope.sortAttribute = null;
        $scope.sortReverse = false;
        $scope.sortBy = function(propertyName) {
            $scope.sortReverse = ($scope.sortAttribute === propertyName) ? !$scope.sortReverse : false;
            $scope.sortAttribute = propertyName;
        };

        $scope.editBadge = function(id) {
            $location.path('/admin/badges/edit/' + id);
        };

        BadgeFactory.getAdminBadges()
        .then(function(badges){
            $scope.badges = badges;
            angular.forEach(badges,function(badge) {
                var date = getDateFromObjectId(badge._id).toLocaleDateString();
                badge.creationDate = date;
            });
        },function(err){
            console.log(err);
            $location.path('/home');
        });
    }]);
})();
