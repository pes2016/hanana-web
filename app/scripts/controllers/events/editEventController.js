(function() {
    'use strict';

    angular.module('hananaWebApp')
    .controller('editEventCtrl', [ '$scope', '$rootScope', '$location', '$http', '$routeParams', 'EventFactory', 'GroupFactory', '$translate',
    function ($scope, $rootScope, $location, $http, $routeParams, EventFactory, GroupFactory, $translate) {

        function errorText(msg) {
            $scope.errorText = msg;
            $scope.formClass = 'has-error';
            $scope.errorClass = 'has-error';
        }
        function success() {
            $scope.formClass = 'has-success';
            $scope.errorClass = 'has-success';
            $location.path('/premium/events');
        }

        $scope.create = false;
        $scope.formClass = '';
        $scope.build = $scope.edit = function () {

            var d = new Date($scope.event.date);
            if (d && d.getTime() < new Date().getTime()) {
                $scope.errorText = $translate.instant('scripts.controllers.createEventController.pastDate');
                $scope.dateError = 'has-error';
                $scope.errorClass = 'has-error';
                return;
            }

            var event = $scope.event;
            EventFactory.editEvent(event)
            .then(function () {
                success();
            }, function(err) {
                errorText(err);
            });
        };

        $rootScope.$on('placeChanged',function(event,loc) {
            $scope.event.lat = loc.lat();
            $scope.event.lon = loc.lng();
        });

        EventFactory.getEvent($routeParams.id)
        .then(function(event){
            event.date = new Date(event.date);
            event.tags = event.tags.join(',');
            event.price = Number(event.price);
            event.age = Number(event.age);
            window.mapProm.then(function() {
                $rootScope.$emit('placeSet', {lon: event.lon, lat: event.lat});
            });
            $scope.event = event;

        }, function(err) {
            errorText(err);
        });

        GroupFactory.getOwnedGroups()
        .then(function(groups){
            $scope.groups = groups;
        });
    }]);
})();
