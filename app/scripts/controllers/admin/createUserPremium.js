(function() {
    'use strict';

    angular.module('hananaWebApp')
    .controller('createUserPremiumCtrl', [ '$scope', '$location', '$http','UserFactory',
    function ($scope, $location, $http, UserFactory ) {

        function errorText(msg) {
            $scope.errorText = msg;
            $scope.formClass = 'has-error';
        }
        function success() {
            $scope.formClass = 'has-success';
            $location.path('/home');
        }

        $scope.formClass = '';
        $scope.createUser = function () {
            UserFactory.createPremium($scope.name, $scope.email)
            .then(function () {
                  success();
            }, function(err) {
                errorText(err);
            });
        };
    }]);
})();
