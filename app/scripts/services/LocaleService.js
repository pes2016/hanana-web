angular.module('hananaWebApp') .service('LocaleService', function ($route, $translate, LOCALES, $rootScope, $window) {
    'use strict';

    var localesObj = LOCALES.locales;
    var _LOCALES = Object.keys(localesObj);
    var _LOCALES_DISPLAY_NAMES = [];
    _LOCALES.forEach(function (locale) {
      _LOCALES_DISPLAY_NAMES.push(localesObj[locale]);
    });

    var currentLocale;

    if ($window.localStorage.getItem('lang')) {
        currentLocale = $window.localStorage.getItem('lang');
    } else {
        currentLocale = $translate.proposedLanguage();
    }

    var checkLocaleIsValid = function (locale) {
      return _LOCALES.indexOf(locale) !== -1;
    };

    var setLocale = function (locale) {
      if (!checkLocaleIsValid(locale)) {
        return;
      }
      currentLocale = locale;

      $window.localStorage.setItem('lang', locale);

      $translate.use(locale);
      $route.reload();
    };

    setLocale(currentLocale);

    $rootScope.$on('$translateChangeSuccess', function (event, data) {
      document.documentElement.setAttribute('lang', data.language);
    });

    return {
      getLocaleHeader: function() {
          return 'lang=' + currentLocale;
      },
      getLocaleDisplayName: function () {
        return localesObj[currentLocale];
      },
      setLocaleByDisplayName: function (localeDisplayName) {
        setLocale(
          _LOCALES[
            _LOCALES_DISPLAY_NAMES.indexOf(localeDisplayName)
            ]
        );
      },
      getLocalesDisplayNames: function () {
        return _LOCALES_DISPLAY_NAMES;
      }
    };
});
