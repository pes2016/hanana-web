'use strict';

/**
 * @ngdoc function
 * @name hananaWebApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the hananaWebApp
 */
angular.module('hananaWebApp')
    .controller('MainCtrl', ['$translate', '$scope', '$location', 'AuthService',
    function ($translate, $scope, $location, AuthService) {
        $scope.changeLanguage = function (langKey) {
          $translate.use(langKey);
        };
        $scope.$watch(AuthService.isLoggedIn, function (value) {
            $scope.logged = (!value) ? false : true;
            if(!value) {
                $location.path('/login');
            } else {
                $scope.admin = AuthService.isAdmin();
                $scope.premium = AuthService.isPremium();
            }
        }, true);
    }]);
