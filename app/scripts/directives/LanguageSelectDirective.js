angular.module('hananaWebApp') .directive('ngTranslateLanguageSelect', function (LocaleService) { 'use strict';

        return {

            restrict: 'A',
            replace: true,
            template: `\
            <div class="language-select" ng-if="visible"> \
                <label> \
                    <input type="image" src="http://www.flags.net/images/largeflags/UNKG0001.GIF" ng-click="changeLanguage('English')" width="50" height="25"/>\
                    <input type="image" src="http://www.flags.net/images/largeflags/SPAN0002.GIF" ng-click="changeLanguage('Español')" width="50" height="25"/>\
                </label>\
            </div>`,

            controller: function ($scope) {
                $scope.currentLocaleDisplayName = LocaleService.getLocaleDisplayName();
                $scope.localesDisplayNames = LocaleService.getLocalesDisplayNames();
                $scope.visible = $scope.localesDisplayNames &&
                $scope.localesDisplayNames.length > 1;

                $scope.changeLanguage = function (locale) {
                    LocaleService.setLocaleByDisplayName(locale);
                };
            }
        };
    });
