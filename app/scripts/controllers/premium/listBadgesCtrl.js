(function() {
    'use strict';

    angular.module('hananaWebApp')
    .controller('listBadgesPremiumCtrl', [ '$scope', '$location', 'BadgeFactory',
    function ($scope, $location, BadgeFactory) {

        function getDateFromObjectId(id) {
            var timestamp = id.substring(0,8);
            return new Date( parseInt( timestamp, 16 ) * 1000);
        }

        $scope.badges = [];
        $scope.sortAttribute = null;
        $scope.sortReverse = false;
        $scope.sortBy = function(propertyName) {
            $scope.sortReverse = ($scope.sortAttribute === propertyName) ? !$scope.sortReverse : false;
            $scope.sortAttribute = propertyName;
        };

        BadgeFactory.getPremiumBadges()
        .then(function(badges){
            $scope.badges = badges;
            angular.forEach(badges,function(badge) {
                var date = getDateFromObjectId(badge._id).toLocaleDateString();
                badge.creationDate = date;
            });
        },function(){
            $location.path('/home');
        });
    }]);
})();
