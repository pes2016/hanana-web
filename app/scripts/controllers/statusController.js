(function() {
    'use strict';

    var app = angular.module('hananaWebApp');

    app.run(['$rootScope', '$location', 'AuthService',
    function ($rootScope, $location, AuthService) {
        $rootScope.$on('$routeChangeStart', function (event) {
            if (!AuthService.isLoggedIn()) {
                if ($location.path() === '/login'){
                    $rootScope.logged = false;
                    return;
                }
                console.log('DENY');
                event.preventDefault();
                $location.path('/login');
            }
            else {
                $rootScope.logged = true;
                if ($location.path().indexOf('/admin') === 0 && !AuthService.isAdmin()) {
                    console.log('DENY');
                    event.preventDefault();
                    //$location.path('/');
                } else if ($location.path().indexOf('/premium') === 0 && !AuthService.isPremium()) {
                    event.preventDefault();
                    console.log('DENY');
                    //$location.path('/');
                }
            }
        });
    }]);
})();
