(function() {
    'use strict';

    angular.module('hananaWebApp')
    .controller('listEventsCtrl', [ '$scope', '$location', 'EventFactory', '$timeout', '$translate',
    function ($scope, $location, EventFactory, $timeout, $translate) {

        function getDateFromObjectId(id) {
            var timestamp = id.substring(0,8);
            return new Date( parseInt( timestamp, 16 ) * 1000);
        }

        function errorText (msg) {
			$scope.errorText = msg;
			$scope.formClass = 'has-error';
		}

        function waitForDownload (iterator, problem, url) {
            if(iterator > 0) {
                $timeout(function(){
                    iterator--;
                    if($scope.url === url){
                        iterator = 0;
                        problem = false;
                    }
                    waitForDownload(iterator, problem, url);
                });
            }
            else {
                if(problem) {
                    errorText($translate.instant('commons.connectionError'));
                }
                else {
                    document.getElementById('qr').click();
                }
            }
        }

        $scope.downloadQR = function(id) {
            EventFactory.downloadQR(id)
            .then(function(url){
                $scope.url = url;
                var iterator = 10;
                var problem = true;
                // Esta funcion esta hecha para esperar a que $scope se actualice.
                // $scope.$apply() no funcionaba porque ya habia uno en curso.
                waitForDownload(iterator, problem, url);
            }, function(err) {
                errorText(err);
            });
        };

        $scope.deleteEvent = function(event) {
            event.deleting = true;
        };

        $scope.deleteEventConfirm = function(id) {
            var eventIndex = $scope.events.findIndex(function(event) {
                return event._id === id;
            });

            if ( $scope.events[eventIndex].dateTime < Date.now()) {
                errorText($translate.instant('scripts.controllers.editEventController.pastEvent'));
            }
            else {
                EventFactory.deleteEvent(id)
                .then(function(){
                    var eventIndex = $scope.events.findIndex(function(event){
                        return event._id === id;
                    });
                    $scope.events.splice(eventIndex,1);
                }, function(err) {
                    errorText(err);
                    var eventIndex = $scope.events.findIndex(function(event){
                        return event._id === id;
                    });
                    $scope.events[eventIndex].deleting = false;
                });
            }
        };

        $scope.deleteEventDeny = function(event) {
            event.deleting = false;
        };

        $scope.events = [];
        $scope.sortAttribute = null;
        $scope.sortReverse = false;
        $scope.sortBy = function(propertyName) {
            $scope.sortReverse = ($scope.sortAttribute === propertyName) ? !$scope.sortReverse : false;
            $scope.sortAttribute = propertyName;
        };

        $scope.editEvent = function(id) {
            var eventIndex = $scope.events.findIndex(function(event) {
                return event._id === id;
            });

            if ( $scope.events[eventIndex].dateTime < Date.now()) {
                errorText($translate.instant('scripts.controllers.editEventController.pastEvent'));
            }
            else {
                $location.path('/premium/events/edit/' + id);
            }
        };

        EventFactory.getEvents()
        .then(function(events){
            $scope.events = events;
            angular.forEach(events,function(event) {
                var creationDate = getDateFromObjectId(event._id).toLocaleDateString();
                event.dateTime = new Date(event.date).getTime();
                event.date = new Date(event.date).toLocaleDateString();
                event.creationDate = creationDate;
                event.deleting = false;
            });
        },function(){
            $location.path('/home');
        });
    }]);
})();
