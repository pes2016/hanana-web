(function() {
    'use strict';

    angular.module('hananaWebApp')
    .controller('groupEditCtrl', [ '$scope', '$location', '$routeParams', 'GroupFactory', '$translate',
    function ($scope, $location, $routeParams, GroupFactory, $translate) {

        function errorText(msg) {
            $scope.errorText = msg;
            $scope.formClass = 'has-error';
        }

        $scope.group = [];

        $scope.save = function() {

            if(!$scope.group.name || !$scope.group.description) {
                errorText($translate.instant('commons.fillAllFields'));
            }
            else {
                var b = false;
                var backup = $scope.group.image;
                if($scope.file) {
                    b = true;
                    $scope.image = '#';
                    $scope.group.image = $scope.file;
                }
                GroupFactory.editGroup($scope.group, b)
                .then(function(){
                    $location.path('/premium/groups');
                },function(err){
                    if(b) {
                        $scope.group.image = backup;
                        $scope.image = backup;
                    }
                    errorText(err);
                });
            }
        };

        GroupFactory.getOwnedGroup($routeParams.id)
        .then(function(group){
            $scope.group = group;
            $scope.image = group.image;
        },function(){
            $location.path('/home');
        });
    }]);
})();
