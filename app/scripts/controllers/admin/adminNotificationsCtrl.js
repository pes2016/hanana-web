(function() {
    'use strict';

    angular.module('hananaWebApp')
    .controller('adminNotificationsCtrl', [ '$scope', '$location', 'NotificationFactory', '$translate',
    function ($scope, $location, NotificationFactory, $translate ) {

        function errorText(msg) {
            $scope.errorText = msg;
            $scope.formClass = 'has-error';
        }
        function success() {
            $scope.formClass = 'has-success';
            $location.path('/home');
        }

        $scope.formClass = '';
        $scope.createNotification = function () {
            if(!$scope.title || !$scope.description) {
                errorText($translate.instant('commons.fillAllFields'));
            }
            else {
                NotificationFactory.createNotificationAdmin($scope.title, $scope.description)
                .then(function () {
                    success();
                }, function(err) {
                    errorText(err);
                });
            }
        };
    }]);
})();
