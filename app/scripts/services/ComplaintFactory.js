(function() {
    'use strict';

    angular.module('hananaWebApp')
    .factory('ComplaintFactory', ['$http', '$q',
    function ($http, $q) {

        return {

            getComplaints: function(page) {
                var prom = $q.defer();

                $http.get('/api/admin/complaints?page=' + page)
                    .then(function (response) {
                        prom.resolve(response.data);
                    },function(err) {
                        prom.reject(err.data);
                    });

                return prom.promise;
            },

            discardComplaint: function(id) {
                var prom = $q.defer();

                $http.delete('/api/admin/discardComplaint/' + id)
                    .then(function () {
                        prom.resolve();
                    },function(err) {
                        prom.reject(err.data);
                    });

                return prom.promise;
            },

            acceptComplaint: function(id, type) {
                var prom = $q.defer();

                $http.delete('/api/admin/acceptComplaint/' + id + '?type=' + type)
                    .then(function () {
                        prom.resolve();
                    },function(err) {
                        prom.reject(err.data);
                    });

                return prom.promise;
            }
        };
    }]);
})();
