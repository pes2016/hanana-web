(function() {
    'use strict';

    angular.module('hananaWebApp')
    .controller('premiumNotificationsCtrl', [ '$scope', '$location', 'NotificationFactory', 'GroupFactory', '$translate',
    function ($scope, $location, NotificationFactory, GroupFactory, $translate ) {

        function errorText(msg) {
            $scope.errorText = msg;
            $scope.formClass = 'has-error';
        }
        function success() {
            $scope.formClass = 'has-success';
            $location.path('/home');
        }

        $scope.formClass = '';
        $scope.createNotification = function () {
            if(!$scope.title || !$scope.description || !$scope.group) {
                errorText($translate.instant('commons.fillAllFields'));
            }
            else {
                NotificationFactory.createNotificationPremium($scope.title, $scope.description, $scope.group)
                .then(function () {
                    success();
                }, function(err) {
                    errorText(err);
                });
            }
        };

        GroupFactory.getOwnedGroups()
        .then(function(groups){
            $scope.groups = groups;
        },function(){
            $location.path('/home');
        });
    }]);
})();
