(function() {
    'use strict';

    angular.module('hananaWebApp')
    .controller('groupPremiumListCtrl', [ '$scope', '$location', 'GroupFactory',
    function ($scope, $location, GroupFactory) {

        function getDateFromObjectId(id) {
            var timestamp = id.substring(0,8);
            return new Date( parseInt( timestamp, 16 ) * 1000);
        }

        $scope.deleteGroup = function(group) {
            group.deleting = true;
        };

        $scope.deleteGroupConfirm = function(id) {
            GroupFactory.deleteGroup(id)
            .then(function(){
                var groupIndex = $scope.groups.findIndex(function(group){
                    return group._id === id;
                });
                $scope.groups.splice(groupIndex,1);
            });
        };

        $scope.deleteGroupDeny = function(group) {
            group.deleting = false;
        };

        $scope.groups = [];
        $scope.sortAttribute = null;
        $scope.sortReverse = false;
        $scope.sortBy = function(propertyName) {
            $scope.sortReverse = ($scope.sortAttribute === propertyName) ? !$scope.sortReverse : false;
            $scope.sortAttribute = propertyName;
        };
        $scope.editGroup = function(id) {
            $location.path('/premium/groups/edit/' + id);
        };

        GroupFactory.getOwnedGroups()
        .then(function (groups) {
            $scope.groups = groups;
            angular.forEach(groups, function(group) {
                var date = getDateFromObjectId(group._id).toLocaleDateString();
                group.creationDate = date;
            });
        });

    }]);
})();
