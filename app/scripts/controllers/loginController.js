(function() {
    'use strict';
    angular.module('hananaWebApp')
    .controller('loginCtrl', [ '$scope', '$location', 'AuthService',
    function ($scope, $location, AuthService) {

        function setSuccess() {
            $scope.errorText = 'Success!';
            $scope.formClass = 'has-success';
            $location.path('/home');
        }

        function clearAndError(msg) {
            $scope.errorText = msg.data;
            $scope.formClass = 'has-error';
        }

        $scope.formClass = '';
        $scope.logUser = function () {
            // Ask to the server, do your job and THEN set the user
            AuthService.login($scope.email,$scope.password)
                .then(function () {
                    // Login successful, redirect to home
                    setSuccess();
                }, function(err) {
                    // Show an error
                    clearAndError(err);
                });

        };
    }]);
})();
