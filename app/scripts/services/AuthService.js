(function() {
  'use strict';

    angular.module('hananaWebApp')
    .factory('AuthService', ['$http', '$q', '$window',
    function ($http, $q, $window) {

        var ls = [];

        function saveTokens (email,access,refresh,admin, premium) {
            if ($window.localStorage) {
                $window.localStorage.setItem('email', email);
                $window.localStorage.setItem('access', access);
                $window.localStorage.setItem('refresh', refresh);
                $window.localStorage.setItem('admin', admin);
                $window.localStorage.setItem('premium', premium);
            } else {
                ls.email = email;
                ls.access = access;
                ls.refresh = refresh;
                ls.admin = admin;
            }
        }

        function resetTokens () {
            if ($window.localStorage) {
                $window.localStorage.removeItem('email');
                $window.localStorage.removeItem('access');
                $window.localStorage.removeItem('refresh');
                $window.localStorage.removeItem('admin');
                $window.localStorage.removeItem('premium');
            } else {
                ls = [];
            }
        }

        function getLS (key) {
            return ($window.localStorage) ? $window.localStorage.getItem(key) : ls[key];
        }

        function setLS (key,val) {
            if ($window.localStorage) {
                $window.localStorage.setItem(key, val);
            } else {
                ls[key] = val;
            }
        }

        function refreshToken () {
            return $http.post('/api/refreshToken', {
                email: getLS('email'),
                refresh_token: getLS('refresh')
            }).then(
                function (response) {
                    setLS('access',response.data.access_token);
                    return $q.resolve(response);
                },
                function (err) {
                    resetTokens();
                    return $q.reject(err);
                }
            );
        }

        return {

            refresh: function() {
                return refreshToken ();
            },

            isLoggedIn: function() {
                return getLS('access') && getLS('refresh') &&  getLS('email') && getLS('admin') && getLS('premium');
            },

            logout: function() {
                resetTokens();
                return true;
            },

            login: function (email,pass) {
                return $http.post('/api/login', {
                    email: email,
                    password: pass
                }).then(
                    function (response) {
                        saveTokens(email,
                            response.data.access_token,
                            response.data.refresh_token,
                            response.data.admin,
                            response.data.premium
                            );
                        return $q.resolve(response);
                    },
                    function (err) {
                        resetTokens();
                        return $q.reject(err);
                    }
                );
            },

            getUser: function() {
                return $http.get('/api/users/me?access_token='+ getLS('access'))
                .then(
                    function (response) {
                        return $q.resolve(response);
                    },
                    function () {
                        return refreshToken();
                    }
                );
            },

            isAdmin: function() {
                return getLS('admin')==='true';
            },

            isPremium: function() {
                return getLS('premium')==='true';
            },

            getAC: function() {
                return 'access_token='+getLS('access');
            }

        };
    }]);
})();
